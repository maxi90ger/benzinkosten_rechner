package benzinkosten_rechner.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn = (Button) findViewById(R.id.buttonRUN);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    EditText ekm = (EditText) findViewById(R.id.editTextKM);
                    EditText evb = (EditText) findViewById((R.id.editTextVB));
                    EditText ep = (EditText) findViewById((R.id.editTextP));
                    EditText emf = (EditText) findViewById((R.id.editTextMF));

                    double km = Double.valueOf(ekm.getText().toString());
                    double vb = Double.valueOf(evb.getText().toString());
                    double p = Double.valueOf(ep.getText().toString());
                    double mf = Double.valueOf(emf.getText().toString());

                    double result = ((km/100.0) * (vb*p));
                    double resultmf = (result / mf);

                    TextView res = (TextView) findViewById(R.id.textViewRS);
                    res.setText(getString(R.string.result, result));

                    TextView resmf = (TextView) findViewById(R.id.textViewRSMF);
                    resmf.setText(getString(R.string.resultmf, resultmf));
                    return;
                }
                catch (NumberFormatException e) {
                    Toast.makeText(MainActivity.this, R.string.error1, Toast.LENGTH_SHORT).show();
                    return;
                }
                catch (Exception e) {
                    Toast.makeText(MainActivity.this, R.string.error2, Toast.LENGTH_SHORT).show();
                    return;
                }
                finally {
                    //...nach catch benötigt um weiter zu coden...//
                }
            }
        });
    }

    private void calculations() throws NumberFormatException, Exception {
        try {
            EditText ekm = (EditText) findViewById(R.id.editTextKM);
            EditText evb = (EditText) findViewById((R.id.editTextVB));
            EditText ep = (EditText) findViewById((R.id.editTextP));
            EditText emf = (EditText) findViewById((R.id.editTextMF));

            double km = Double.valueOf(ekm.getText().toString());
            double vb = Double.valueOf(evb.getText().toString());
            double p = Double.valueOf(ep.getText().toString());
            double mf = Double.valueOf(emf.getText().toString());

            double result = ((km/100.0) * (vb*p));
            double resultmf = (result / mf);

            TextView res = (TextView) findViewById(R.id.textViewRS);
            res.setText(getString(R.string.result, result));

            TextView resmf = (TextView) findViewById(R.id.textViewRSMF);
            resmf.setText(getString(R.string.resultmf, resultmf));
            return;
        }
        catch (NumberFormatException e) {
            Toast.makeText(MainActivity.this, R.string.error1, Toast.LENGTH_SHORT).show();
            return;
        }
        catch (Exception e) {
            Toast.makeText(MainActivity.this, R.string.error2, Toast.LENGTH_SHORT).show();
            return;
        }
        finally {
            /*if (ekm.getText().toString().isEmpty() == true || evb.getText().toString().isEmpty() == true || ep.getText().toString().isEmpty() == true || emf.getText().toString().isEmpty() == true) {
            Toast.makeText(MainActivity.this, R.string.error1, Toast.LENGTH_LONG).show();
            return;
            }*/
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_verlauf) {
            Intent verlauf = new Intent(this, Verlauf.class);
            startActivity(verlauf);
            return true;
        }
        if (id == R.id.action_settings) {
            Intent about = new Intent(this, About.class);
            startActivity(about);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
